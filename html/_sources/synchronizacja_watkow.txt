*********************
Synchronizacja wątków
*********************


Poprawność programów współbieżnych
==================================

Właściwości związane z poprawnością programu współbieżnego:

* **Właściwość żywotności** - program współbieżny jest żywotny, jeśli zapewnia, że każde pożądane zdarzenie w końcu zajdzie.
* **Właściwość bezpieczeństwa** - program współbieżny jest bezpieczny, jeśli nigdy nie doprowadza do niepożądanego
  stanu problem wzajemnego wykluczania problem producentów i konsumentów - każda porcja danych zostanie skonsumowana w kolejności ich produkowania.

  - problem wzajemnego wykluczania -
  - problem producentów i konsumentów - każda porcja danych zostanie skonsumowana w kolejności ich produkowania.

Bezpieczeństwo wątków w programowaniu współbieżnym (*thread-safety*) jest pojęciem stosowanym w kontekście programów wielowątkowych.

Fragmenty kodu są "wątkowo-bezpieczne" (*thread-safe*), jeżeli funkcje wywoływane jednocześnie przez wiele wątków wykonują się prawidłowo

Niepoprawnie zaprojektowane zależności między wątkami często naruszają integralność i żywotność danych


Sposoby osiągnięcia bezpieczeństwa:

* Niezmienność danych (*Immutability*)
* Atomowość (*Atomicity*)
* Jawne blokowanie (*Explicit Locking*)



Współdzielenie obiektów biblioteki standardowej
-----------------------------------------------

.. attention::

    Współbieżny dostęp do zmiennych, w czasie gdy przynajmniej jeden wątek modyfikuje stan współdzielonego obiektu
    (**data race**) może skutkować **niezdefiniowanym zachowaniem** (*undefined behaviour*)


Dla kontenerów STL oraz adapeterów kontenerów:

* Dozwolony jest współbieżny odczyt (operacje *read-only*) np:

  - wywołanie metod ``begin()``, ``end()``, ``rbegin()``, ``rend()``, ``front()``, ``back()``, ``lower_bound()``
    ``upper_bound()``, ``equal_range()``, ``at()``
  - ``operator []`` - z wyjątkiem kontenerów asocjacyjnych
  - dostęp za pomocą iteratorów, jeśli nie modyfikujemy stanu wskazywanych elementów

* Dozwolony jest współbieżny dostęp do różnych elementów kontenera

  - za wyjątkiem obiektów ``bitset`` oraz ``vector<bool>``


Dla formatowanych operacji wejścia oraz wyjścia na obiektach strumieni standardowych (``cin``, ``cout``, ``cerr``, ...)

* Dozwolony jest dostęp współbieżny
* Może wystąpić przemieszanie kolejności znaków (*interleaved characters*)


Sekcja krytyczna
================

Fragment kodu programu, który w danej chwili może być wykonywany tylko przez jeden wątek. Sekcja krytyczna zapewnia
właściwość wzajemnego wykluczenia.

Standardową realizacją wzajemnego wykluczenia jest obiekt blokady zawierający operacje

* ``lock()``
* ``unlock()``

Wątek

* pozyskuje blokadę (blokuje), kiedy wywołuję metodę ``lock()``
* zwalnia blokadę (odblokowuje), gdy wywołuje metodę ``unlock()``


Wątek jest prawidłowo skonstruowany, jeżeli spełnia poniższe warunki:

* Każda sekcja krytyczna jest skojarzona z niepowtarzalnym obiektem blokady
* Wątek, próbując wejść do sekcji krytycznej, wywołuje metodę ``lock()`` tego obiektu
* Wątek wychodząc z sekcji krytycznej, wywołuje metodę ``unlock()``


Podstawowe obiekty blokad
-------------------------

* Mutexy (*Mutual Exclusion*) - obiekty implementujące właściowość wzajemnego wykluczania
* Semafory - binarne i zliczające
* Blokady współdzielone (blokady *readers-writers*)


Warunkowe wzajemne wykluczenie
------------------------------

* Zmienne warunkowe używane są do powiadamiania wątków o zaistnieniu określonego warunku.
* Są powiązane z muteksem, który jest ponownie pozyskiwany w momencie wybudzenia z blokady
* Warunek wybudzenia z blokady jest zdefiniowany przy pomocy predykatu, który jest parametrem blokady


Muteksy i klasy zarządzające muteksami
======================================

Obiekt muteksu jest implementacją blokady w bibliotece standardowej C++11.
Zapewnia implementację operacji wzajemnego wykluczenia (*mutual exclusion*).
Umożliwia ochronę współdzielonych zmiennych przed sytuacją wyścigu (*race condition*).

* Wątek pozyskuje muteks (wchodzi do sekcji krytycznej) wywołując na rzecz muteksu metodę ``lock()``.
* Wątek opuszcza sekcję krytyczną - zwalnia muteks - wywołując metodę ``unlock()``

.. code-block:: c++

  int counter = 0;
  std::mutex mtx;

  void task()
  {
      mtx.lock();

      counter++;

      mtx.unlock();
  }

  std::thread thd1(task);
  std::thread thd2(task);


Koncepty obiektów blokowalnych
------------------------------

Muteksy zaimplementowane w bibliotece standardowej korzystają z następujących konceptów obiektów blokowalnych (*lockalable objects*)

* BasicLockable
* Lockable
* TimedLockable
* SharedLockable

Koncepty BasicLockable, Lockable i klasa ``std::mutex``
-------------------------------------------------------

Koncept ``BasicLockable`` modeluje właściwość wzajemnego wykluczenia. Typ L jest zgodny z wymaganiami konceptu ``BasicLockable``
gdy w interfejsie posiada metody:

* ``void lock()`` - biężący wątek jest wstrzymany, aż do momentu pozyskania blokady
* ``void unlock()`` - jeżeli bieżący wątek jest posiadaczem blokady, to następuje jej zwolnienie


Koncept ``Lockable`` jest rozszerza koncept ``BasicLockable`` o wymóg posiadania metody:

* ``bool try_lock()`` - próba pozyskania blokady bez wstrzymywania bieżącego wątku. Zwraca ``true`` jeśli blokada
  została pozyskana, w przeciwnym wypadku zwraca``false``.


Klasa ``std::mutex`` implementuje koncept *Lockable* zapewniając podstawowy mechanizm synchronizacji:

* Implementuje nie-rekursywną werję muteksu


Klasa ``std::recursive_mutex``

* wersja rekursywna konceptu Lockable

TimedLockable i klasa ``std::timed_mutex``
------------------------------------------

Koncept TimedLockable wzbogaca koncept Lockable o metody umożliwiające zdefiniowanie maksymalnego czasu oczekiwania na pozyskanie blokady przez wątek

* ``bool try_lock_until(abs_time)``
* ``bool try_lock_for(rel_time)``

Implementacja - ``std::timed_mutex`` oraz ``std::recursive_timed_mutex``

Blokady współdzielone (C++14)
=============================

Blokady współdzielone wprowadzone są w standardzie C++14.

Muteksy implementujące koncepty Lockable oraz TimedLockable szeregują odwołania do zasobów, lecz nie rozróżniają dostępów modyfikujących od niemodyfikujących.

Odczyt danych współdzielonych:

* nie może zakłócić innego odczytu współbieżnego
* pozyskiwane są blokady współdzielone

Przed podjęciem próby zapisu danych współdzielonych należy pozyskać blokadę na wyłączność.


Koncept SharedLockable
----------------------

Koncept SharedLockable rozszerza koncept Lockable o możliwość pozyskiwania blokad współdzielonych (*shared ownership*) przy pomocy metod:

* ``void lock_shared()``
* ``bool try_lock_shared()``
* ``bool try_lock_shared_for(rel_time)``
* ``bool try_lock_shared_until(abs_time)``
* ``void unlock_shared()``

Implementacja - ``std::shared_mutex``


Klasa ``std::shared_mutex``
---------------------------

Blokady współdzielone są implementowane przez klasę ``std::shared_mutex``

Do pozyskania wyłącznej blokady przed rozpoczęciem operacji zapisu należy użyć:

* ``std::lock_guard<std::shared_mutex>``
* ``std::unique_lock<std::shared_mutex>``


Do pozyskiwania współdzielonych blokad w trakcie czytania danych należy użyć:

* ``std::shared_lock<std::shared_mutex>``

.. code-block:: c++

    int slots[size];
    std::shared_mutex mtx;

    void reader()
    {
        int index = get_slot_index();

        std::shared_lock<std::shared_mutex> lock(mtx);
        int value = slots[index];
        lock.unlock();

        process_value();
    }

    void writer()
    {
        int index = get_slot_index();

        std::lock_guard<std::shared_mutex> lock(mtx);
        slots[index] = new_value();
    }


Managery blokad
===============

Zarządzanie blokadami (obiektami muteksów) odbywa się za pomocą następujących klas:

* ``lock_guard<>``
* ``unique_lock<>``
* ``shared_lock<>``

Wszystkie klasy zarządzające blokadami implementują mechanizm *RAII* gwarantujący zwolnienie posiadanej przez wątek blokady w przypadku zgłoszenia sytuacji wyjątkowej.
W destruktorze wywoływana jest metoda ``unlock()``.


Klasa ``std::lock_guard<>``
---------------------------

Manager blokad implementuje technikę RAII w celu zarządzania zasobem w postaci muteksu.

.. code-block:: c++

    template <class Mutex>
    class lock_guard
    {
    public:
        typedef Mutex mutex_type;

        explicit lock_guard(mutex_type& m);
        lock_guard(mutex_type& m, adopt_lock_t);
        ~lock_guard();

        lock_guard(lock_guard const&) = delete;
        lock_guard& operator=(lock_guard const&) = delete;
    private:
        mutex_type& mtx_;
    };


``std::lock_guard`` jest najprostszym managerem blokad:

* konstruktor pozyskuje blokadę wywołując na rzecz muteksu przekazanego jako argument metodę ``lock()``
* destruktor zwalnia blokadę wywołując ``unlock()``
* przeciążony konstruktor przyjmujący jako drugi parametr  obiekt typu ``std::adopt_lock_t`` umożliwia
  adaptowanie, przejęcie prawa własności i tym samym odpowiedzialności za zwolnienie pozyskanej już wcześniej blokady

  - ``constexpr defer_lock_t defer_lock {};`` - nie pozyskuje na własność blokady
  - ``constexpr try_to_lock_t try_to_lock { };`` - próba pozyskania blokady bez blokowania
  - ``constexpr adopt_lock_t adopt_lock { };`` - muteks został już pozyskany wcześniej


Przykład:

.. code-block:: c++

    std::mutex m;
    unsigned counter=0;

    unsigned increment()
    {
        std::lock_guard<std::mutex> lk(m);
        return ++counter;
    }

    unsigned query()
    {
        std::lock_guard<std::mutex> lk(m);
        return counter;
    }


Klasa ``std::unique_lock<>``
----------------------------

Rozbudowana wersja managera blokad umożliwiająca:

* ochronę *RAII* przed wyciekami blokad
* opóźnione pozyskiwanie blokad
* adaptowanie pozyskanej przez wątek blokady - transfer prawa własności
* podejmowanie nieblokujących prób pozyskania blokady
* korzystanie z muteksów czasowych

Jeśli instancja managera ``std::unique_lock<>`` posiada blokadę:

* metoda ``mutex()`` zwraca wskaźnik do muteksu realizującego blokadę
* metoda ``owns_lock()`` zwraca wartość true
* w momencie niszczenia obiektu destruktor wywoła metodę ``unlock()``

Opóźnione pozyskiwanie blokady – ``std::shared_lock(Lockable& m, std::defer_lock_t)``

* metoda ``owns_lock()`` zwraca ``false``
* metoda ``mutex()`` zwraca adres obiektu muteksu

Adaptowanie pozyskanej przez wątek blokady - ``unique_lock(Lockable& m, std::adopt_lock_t)``

Nieblokujące  próby pozyskania blokady - ``unique_lock(Lockable & m,std::try_to_lock_t)``

* w konstruktorze wywoływana jest dla muteksu nieblokująca metoda ``try_lock()``
* jeśli blokada zostanie pozyskana metoda ``owns_lock()`` zwraca ``true``, w przeciwnym wypadku zwraca ``false``

Jeśli do konstruktora instancji ``unique_lock<>`` zostanie przekazany muteks implementujący koncept TimedLockable (np. ``std::timed_mutex``) możliwe jest blokujące pozyskiwanie blokad:

* albo przez dany czas
* albo do określonego punktu w czasie

.. code-block:: c++

    std::timed_mutex mutex;
    std::unique_lock<std::timed_mutex> lock(mutex, std::try_to_lock);

    //...
    if (!lock.owns_lock())
    {
        int count = 0;
        do
        {
            std::cout << "Thread doesn't own a lock... Tries to acquire a mutex..."
                << std::endl;
        } while(!lock.try_lock_for(std::chrono::seconds(1)));
    }


Transfer blokad ``std::unique_lock<>``
**************************************

Managery blokad typu ``std::unique_lock<>`` potrafią między sobą transferować własności blokady zgodnie z semantyką
przenoszenia.

.. code-block:: c++

    std::unique_lock<std::mutex> acquire_lock()
    {
        static std::mutex m;
        return std::unique_lock<std::mutex>(m);
    }


Zakleszczenie
=============

Zakleszczenie (*deadlock*)
    sytuacja, w której co najmniej dwa różne wątki czekają na siebie nawzajem, więc żadna nie może się zakończyć.

Do zakleszczenia może dojść w sytuacji istnieją przynajmniej dwa zasoby i dwa wątki ubiegające się o dostęp do tych zasobów.


Przykład:

Klasa ze stanem wewnętrznym, chroniona muteksem. Chcemy napisać operator porównania.

.. code-block:: c++

    class X
    {
        mutable std::mutex the_mutex;
        int some_data;
    public:
        bool operator<(const X& other)
        {
            std::lock_guard<std::mutex> lk(the_mutex);
            std::lock_guard<std::mutex> lk(other.the_mutex);
            return some_data < other.some_data;
        }
    };

Mamy dwa obiekty ``x1`` i ``x2`` i dwa wątki próbujące je porównać, ale w odwrotną stronę:

+---------------------+---------------------+
| **Wątek A**         | **Wątek B**         |
+=====================+=====================+
| ``if(x1 < x2)`` ... | ``if(x2 < x1)`` ... |
+---------------------+---------------------+

Dwa wątki pozyskają muteksy w odwrotnej kolejności, co może spowodować zakleszczenie.

Zapobieganie zakleszczeniom - ``std::lock()``
---------------------------------------------

Metoda ``std::lock`` gwarantuje zablokowanie wszystkich muteksów bez zakleszczenia niezależnie od ich kolejności.

.. code-block:: c++

    bool X::operator< (const X& other)
    {
        std::unique_lock<std::mutex> l1(the_mutex, std::defer_lock);
        std::unique_lock<std::mutex> l2(other.the_mutex, std::defer_lock);
        std::lock(l1, l2);
        return some_data < other.some_data;
    }


Istnieje możliwość skonstruowania blokady typu ``std::unique_lock<>`` bez blokowania za pomocą parametru ``std::defer_lock``.
Pozwala uniknąć zakleszczeń dla blokad uzyskiwanych jednocześnie. Działa na każdym obiekcie implementującym koncept Lockable.

Wciąż może istnieć zagrożenie zakleszczeniem, jeśli blokady są pozyskiwane oddzielnie. Aby zminimalizować ryzyko należy pozyskiwać blokady zawsze w tej samej kolejności.


Synchronizacja pracy wielu wątków
=================================

Często zachodzi potrzeba zsynchronizowania pracy wielu wykonywanych współbieżnie zadań.

Naiwna implementacja komunikacji przy pomocy flagi ``data_ready``:

.. code-block:: c++

    volatile bool data_ready;
    std::mutex mtx;

    void set_data_ready()
    {
        prepare_data();
        std::unique_lock<std::mutex> lk(mtx);
        data_ready = true;
    }

    void task()
    {
        std::unique_lock<std::mutex> lk(mtx);

        while(!data_ready)
        {
            lk.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            lk.lock();
        }

        process_data();
    }


Zmienne warunkowe
=================

Zmienne warunkowe umożliwiają efektywne komunikowanie ze sobą pracujących współbieżnie wątków.
Zapewniają mechanizm międzywątkowych powiadomień o zajściu określonego warunku.

Biblioteka standardowa C++11 dostarcza dwie implementacje zmiennych warunkowych:

* ``std::condition_variable``
* ``std::condition_variable_any``

Obie klasy współpracują z muteksem, aby zapewnić prawidłową synchronizację wątków:

* ``conditional_variable`` współpracuje tylko z typem ``std::mutex``
* ``conditional_variable_any`` współpracuje z dowolnym typem muteksu

.. code-block:: c++

    std::mutex m;
    std::condition_variable ready_cv;
    bool data_ready = false;
    void process_data();

    void task1()
    {
        std::unique_lock<std::mutex> lk(m);
        while(!data_ready)
        {
            ready_cv.wait(lk);
        }
        process_data();
    }

    void task2()
    {
        {
            std::lock_guard<std::mutex> lk(m);
            data_ready = true;
        } // release lock

        ready_cv.notify_one();
    }


Można uprościć kod oczekujący na spełnienie warunku korzystając z przeciążonej wersji metody ``wait(lock_type& lock, predicate_type pred)`` przyjmującej jako argument predykat.

Predykat może być zdefiniowany jako:

* funkcja ``bool pred()``
* obiekt funkcyjny
* obiekt funkcyjny zdefiniowany w momencie wywołania przy pomocy funkcji lambda

.. code-block:: c++

    void task2()
    {
        std::unique_lock<std::mutex> lk(m);
        cond.wait(lk, [=] { return is_ready; });
        process_data();
    }


Powiadomienie o zdarzeniu może zostać zrealizowane przy pomocy dwóch metod:

* ``void notify_one()`` – odblokowuje jeden z wątków znajdujących się w stanie oczekiwania po uprzednim wywołaniu na obiekcie zmiennej warunkowej metody wait()
* ``void notify_all()`` – odblokowuje wszystkie wątki znajdujące się w stanie oczekiwania
