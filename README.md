## Multithreading programming in C++

### Additonal information

#### login and password for VM:

```
dev  /  dev
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

**APT**
```
/etc/apt/apt.conf

Acquire::http::proxy "http://10.144.1.10:8080/";
Acquire::https::proxy "https://10.144.1.10:8080/";
Acquire::ftp::proxy "ftp://10.144.1.10:8080/";
```

#### GIT

```
git clone https://bitbucket.org/infotraining/cpp-thd-2014-11-05-mk
```

[git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

```
git status  # local repository status
git pull    # getting files from repository
git stash   # moving modifications to stash
```

### Linki

http://www.1024cores.net/
http://preshing.com/
http://herbsutter.com/
http://www.cs.wustl.edu/~schmidt/ACE.html
https://software.intel.com/en-us/intel-tbb
http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array
http://blog.bfitz.us/?p=491 - fibers, cooperative scheduling
http://lwn.net/Articles/250967/ - What every programmer should know about memory
http://www.drdobbs.com/article/print?articleId=204801163&siteSectionName=parallel
