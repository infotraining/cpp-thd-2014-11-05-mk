#include <atomic>
#include <iostream>
#include <thread>
#include <vector>

std::atomic_flag f;

class spin_barrier
{
    std::atomic<unsigned int> num_wait;
    std::atomic<unsigned int> generation;
    int barrier;

public:

    spin_barrier(int value):  num_wait(0), generation(0), barrier(value) {}

    void wait()
    {
        int current_generation = generation.load( memory_order::relax );

        if ( num_wait.fetch_add(1) == (barrier - 1))
        {
            num_wait.store(0);
            generation.fetch_add(1);
        }
        else
        {
            while( current_generation == generation.load() );
        }
    }
};

spin_barrier b(2);

void foo(int id)
{
    std::cout << "before " << id << std::endl;
    b.wait();
    std::cout << "after " << id << std::endl;
}

int main()
{
    std::vector<std::thread> t;

    t.emplace_back(foo, 1);
    t.emplace_back(foo, 2);
    t.emplace_back(foo, 3);
    t.emplace_back(foo, 4);
    t.emplace_back(foo, 5);
    t.emplace_back(foo, 6);

    for (auto& th : t)
        th.join();

}
