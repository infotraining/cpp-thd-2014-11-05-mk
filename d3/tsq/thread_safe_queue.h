#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <mutex>
#include <condition_variable>
#include <queue>
#include <memory>



template<typename T>
class thread_safe_queue
{
    std::queue<T> queue_;
    std::mutex mtx_;
    std::condition_variable cv_item_pushed_;
public:
    thread_safe_queue(){}

    thread_safe_queue(const thread_safe_queue& source)
    {
        std::lock_guard<std::mutex> lk{source.mtx_};
        queue_ = source.queue_;
    }

    void push(const T& item)
    {
        std::lock_guard<std::mutex> lk{mtx_};
        queue_.push(item);
        cv_item_pushed_.notify_one();
    }

    void push(T&& item)
    {
        std::lock_guard<std::mutex> lk{mtx_};
        queue_.push(std::move(item));
        cv_item_pushed_.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_};
        cv_item_pushed_.wait(lk, [this] { return !queue_.empty();});

        item = queue_.front();
        queue_.pop();
    }

    std::shared_ptr<T> pop()
    {
        std::unique_lock<std::mutex> lk{mtx_};
        cv_item_pushed_.wait(lk, [this] { return !queue_.empty();});

        auto ptr_item = std::make_shared<T>(queue_.front());
        queue_.pop();

        return ptr_item;
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_, std::try_to_lock};

        if (lk.owns_lock() && !queue_.empty())
        {
            item = queue_.front();
            queue_.pop();
            return true;
        }

        return false;
    }

    bool empty()
    {
        std::lock_guard<std::mutex> lk{mtx_};
        return queue_.empty();
    }
};

#endif // THREAD_SAFE_QUEUE_HPP
