#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<long> q;
mutex c_mtx;

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
    {
        if (n % div == 0)
            return false;
    }
    return true;
}

void producer()
{
    for (long i = 1 ; i < 100; ++i)
    {
        //cout <<"prod" << i <<endl;
        q.push(i);
        this_thread::sleep_for( chrono::milliseconds(100) );
    }

    q.push(-1);
    q.push(-1);
}

void consumer(int id)
{
    for(;;)
    {
        long msg;
        q.pop(msg);

        if (msg == -1)
            return;

        if (is_prime(msg))
        {
            cout << id << " just got " << msg << endl;
        }
    }
}

int main()
{
    thread th_prod(producer);
    thread thd1(consumer, 1);
    thread thd2(consumer, 2);

    th_prod.join();
    thd1.join();
    thd2.join();

    return 0;
}

