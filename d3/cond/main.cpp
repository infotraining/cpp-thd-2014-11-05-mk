#include <iostream>
#include <thread>
#include <mutex>
#include <atomic>
#include <vector>
#include <algorithm>
#include <condition_variable>

using namespace std;

class Data
{
    vector<int> data_;
    atomic<bool> is_ready;


public:

    void read()
    {
        this_thread::sleep_for( chrono::milliseconds(100) );

        data_.resize(100);
        generate(data_.begin(), data_.end(), [] { return rand() % 100; } );

        is_ready.store( true );
    }

    void process()
    {
        while( !is_ready.load() );

        long sum = accumulate(data_.begin(), data_.end(), 0);

        cout << "sum " << sum << endl;
    }
};

class Data_cond
{
    vector<int> data_;

    bool is_ready = false;
    mutex cv_mtx_;
    condition_variable cv_;

public:

    void read()
    {
        cout << "READ" <<endl;
        this_thread::sleep_for( chrono::milliseconds(10000) );

        data_.resize(100);
        generate(data_.begin(), data_.end(), [] { return rand() % 100; } );

        unique_lock<mutex> uql(cv_mtx_);
        is_ready = true;
        uql.unlock();

        cv_.notify_all();
    }

    void process(int id)
    {
        cout << "PROCESS" << id <<endl;
        unique_lock<mutex> lk(cv_mtx_);

        cv_.wait(lk, [this] {return is_ready;});

        long sum = accumulate(data_.begin(), data_.end(), 0);
        cout << id << " sum " << sum << endl;
    }
};


int main()
{
    Data_cond data;

    thread t1( [&data] { data.read(); });
    thread t2( bind( &Data_cond::process, &data, 1) );
    thread t3( [&data] { data.process(2); });
    thread t4( [&data] { data.process(3); });

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    return 0;
}









