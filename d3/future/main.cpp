#include <iostream>
#include <future>
#include <thread>
#include <stdexcept>
#include <string>

using namespace std;


int calculate(int x)
{
    this_thread::sleep_for( chrono::milliseconds(1453) );

    //throw runtime_error("ERRROR#4");

    return x*x;
}

string download_file(const string& url)
{
    this_thread::sleep_for( chrono::milliseconds(2000) );
    return "Content: " + url;
}

shared_future<string> download_async(const string& url)
{
    packaged_task< string()> task( [url] { return download_file(url); });
    shared_future<string> sf( task.get_future() );
    thread thd( move(task) );
    thd.detach();

    return sf;
}

int main()
{
    shared_future<string> f1 = download_async("www.www.www");

    shared_future<string> f2 = f1;

    cout << f1.get() << endl;
    cout << f2.get() << endl;

}

int main_old()
{

    std::packaged_task< int(int) > pt1( calculate );
    std::packaged_task< int() >    pt2( [] { return calculate(12); } );
    std::packaged_task< int() >    pt3( bind(&calculate, 11) );

    cout << "1" << endl;
    pt1.get_future();
    cout << "2" << endl;
    //future<int> f1 = pt1.get_future();

    future<int> f2 = pt2.get_future();
    cout << "3" << endl;
    future<int> f3 = pt3.get_future();
    cout << "4" << endl;



    thread t1( move(pt1), 5 );
    thread t2( move(pt2) );
    thread t3( move(pt3) );

    cout << f1.valid() << endl;

    /*
    try
    {
        cout << f1.get() << endl;
        cout << f2.get() << endl;
        cout << f3.get() << endl;
    }
    catch( const runtime_error& e)
    {
        cout << e.what() << endl;
    }*/

    t1.join();
    t2.join();
    t3.join();

    return 0;
}

