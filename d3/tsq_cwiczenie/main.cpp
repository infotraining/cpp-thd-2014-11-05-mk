#include <iostream>
#include <queue>

using namespace std;

queue<long> q;

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
    {
        if (n % div == 0)
            return false;
    }
    return true;
}

void producer()
{
    for (long i = 1 ; i < 100; ++i)
    {
        q.push(i);
    }
}

void consumer(int id)
{
    while( !q.empty() )
    {
        if (is_prime(msg))
        {
            cout << "#" << id << " just got " << msg << endl;
        }
    }
}

int main()
{
    producer();
    consumer(1);
}

