#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <queue>
#include <condition_variable>
#include <mutex>
#include <vector>

template <typename T>
class thread_safe_queue
{
    std::queue<T> q;

public:
    thread_safe_queue() {}


    void push(const T& item) {}
    void pop(T& item) {}

    bool empty() {return true;}
};

#endif // THREAD_SAFE_QUEUE_H
