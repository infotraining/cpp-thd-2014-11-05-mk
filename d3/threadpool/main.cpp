#include <iostream>
#include <thread>
#include <chrono>
#include <functional>
#include <vector>
#include "thread_safe_queue.hpp"


using namespace std;

typedef std::function<void()> Task;

class ThreadPool
{
public:
    ThreadPool(size_t no_of_threads)
    {
        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            submit(END_OF_WORK);

        for(auto& t : threads_)
            t.join();
    }

    void submit(Task f)
    {
        task_queue_.push(f);
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    std::vector<std::thread> threads_;

    const static nullptr_t END_OF_WORK;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            if (task == END_OF_WORK)
                return;

            task();
        }
    }

    bool is_end_of_work(Task t)
    {
        return t == nullptr;
    }
};

const nullptr_t ThreadPool::END_OF_WORK = nullptr;

void background_task(int id)
{
    cout << "BT#" << id << " starts..." << endl;
    this_thread::sleep_for(chrono::milliseconds(rand() % 3000));
    cout << "BT#" << id << " ends..." << endl;
}

int main()
{
    ThreadPool pool(8);

    pool.submit([] { background_task(1); }); // void()
    pool.submit([] { background_task(2); });

    for(int i = 3; i <= 20; ++i)
        pool.submit(bind(&background_task, i)); // void()
}
