#include <iostream>
#include <string>
#include <thread>
#include <future>
#include <chrono>

using namespace std;

typedef std::string FileContent;

FileContent download_file(const string& url)
{
    this_thread::sleep_for(chrono::seconds(2));

    return "content of file: " + url;
}

shared_future<FileContent> download_file_async(const string& url)
{
    packaged_task<FileContent ()> task([=] { return download_file(url); });
    shared_future<FileContent> sf(task.get_future());
    thread thd(move(task));
    thd.detach();
    return sf;
}

int main()
{
    shared_future<FileContent> f1 =
        download_file_async("http://infotraining.pl");

    shared_future<FileContent> f2 = f1; // kopiowanie obiektu shared_future

    std::cout << "f1: " << f1.get() << std::endl;
    std::cout << "f2: " << f2.get() << std::endl;
}

