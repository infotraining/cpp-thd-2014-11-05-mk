#include <iostream>
#include <thread>
#include <functional>
#include <vector>

using namespace std;

void foo()
{
    cout << "hello from foo" << endl;
}


class Bar
{
public:

      void operator() ()
      {
          cout << "hello from bar" << endl;
      }

      void generate()
      {
          cout << "generate" << endl;
      }

      void consume()
      {
          cout << "consume" << endl;
      }
};


int main()
{
    cout << "Main thread starts..." << endl;

    int x = 42;

    thread t1(foo);
    thread t2{ Bar() };
    Bar b;
    thread t3(b);

    thread t4( bind(&Bar::generate, b ) );
    thread t5( &Bar::consume, b );
    thread t6( [&x]{ cout << "hello from lambda - " << x << endl;} );

    //vector<thread> threads = {thread([]{})} ;

    vector<thread> threads;

    threads.emplace_back( [] { cout << "hello from emplace" << endl; }  );

    threads.push_back( move(t1) );

    threads.push_back( thread( [] {}) );

    {
        Bar b;

        b.do_smth();
    }

    for(auto &t : threads)
        t.join();

    t6.join();
    t5.join();
    t4.join();
    t3.join();
    t2.join();

    if ( t1.joinable() )
        t1.join();
}











