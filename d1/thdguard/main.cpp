#include <iostream>
#include <thread>

using namespace std;


class ThreadGuard
{
    thread& thd_;

public:

    ThreadGuard(thread& t) : thd_(t) {}

    ThreadGuard(const ThreadGuard&) = delete;
    ThreadGuard& operator=(const ThreadGuard&) = delete;

    ~ThreadGuard()
    {
        if( thd_.joinable() )
            thd_.join();
    }
};


class ScopedThread
{
    thread thd_;

public:

    ScopedThread(thread&& thd) : thd_(move(thd)) {}

    ScopedThread(const ScopedThread&) = delete;
    ScopedThread& operator=(const ScopedThread&) = delete;

    ScopedThread(ScopedThread&&) = default;
    ScopedThread& operator=(ScopedThread&&) = default;

    template <typename... T>
    ScopedThread(T&&... args) : thd_(  forward<T>(args)... )
    {
    }

    void join()
    {
        thd_.join();
    }

    ~ScopedThread()
    {
        if(thd_.joinable())
            join();
    }
};


void local_scope()
{
    thread t( [] { cout << "thread, local scope" << endl; } );
    ThreadGuard thd_guard(t);

    ScopedThread( [] { cout << "advanced thread guard" << endl; });
}


void bar()
{
   cout << hex << "0x" <<this_thread::get_id() << endl;

   this_thread::sleep_for( chrono::milliseconds(1500) );
}

thread create_thread()
{
    thread thd(bar);

    return thd;
}

int main()
{
    cout << "Hello World!" << endl;

    local_scope();

    thread t1 = create_thread();
    thread t2 = create_thread();

    t1.join();
    t2.join();

    return 0;
}

