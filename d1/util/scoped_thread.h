#pragma once
#include <thread>

class scoped_thread
{
    thread thd_;

public:

    scoped_thread(thread&& thd) : thd_(move(thd)) {}

    scoped_thread(const scoped_thread&) = delete;
    scoped_thread& operator=(const scoped_thread&) = delete;

    scoped_thread(scoped_thread&&) = default;
    scoped_thread& operator=(scoped_thread&&) = default;

    template <typename... T>
    scoped_thread(T&&... args) : thd_( forward<T>(args)... )
    {
    }

    void join()
    {
        thd_.join();
    }

    ~scoped_thread()
    {
        if(thd_.joinable())
            join();
    }
};
