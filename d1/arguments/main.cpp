#include <iostream>
#include <thread>
#include <random>
#include <algorithm>

using namespace std;

void test( int& r)
{
    this_thread::sleep_for( chrono::milliseconds(100) );
    cout << r << endl;
}

thread ref_test()
{
    int value = 42;
    return thread(test, ref(value));   // wiszaca referencja
}

int main()
{
    int x=11;
    thread t1( bind(test, x) );

    thread t2 = ref_test();

    t1.join();
    t2.join();

    return 0;
}

