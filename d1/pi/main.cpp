#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>

using namespace std;

long N = 100000000;

int count_inside_circle(long N, long& counter)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1, 1);

    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if ( x*x + y*y < 1)
            ++counter;
    }
}

int atomic_count_inside_circle(long N, atomic<long>& counter)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1, 1);

    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if ( x*x + y*y < 1)
            ++counter;
    }
}

int mutex_count_inside_circle(long N, long& counter)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1, 1);

    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if ( x*x + y*y < 1)
        {
            mtx.lock();
            ++counter;
            mtx.unlock();
        }
    }
}


int multithread_pi_calc()
{
    auto start = chrono::high_resolution_clock::now();
    int hwc = thread::hardware_concurrency();
    vector<thread> threads;
    vector<long> counters(hwc);
    // creating threads
    for (long& counter : counters)
    {
        threads.emplace_back(count_inside_circle, N/hwc, ref(counter));
    }
    // joining threads
    for(auto& th : threads) th.join();
    long counter = accumulate(counters.begin(), counters.end(), 0L);
    cout << "Pi = " << double(counter)/double(N)*4 << " time = ";
    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms - multithread" << endl;
}



int multithread_pi_calc_global_counter()
{
    auto start = chrono::high_resolution_clock::now();
    int hwc = thread::hardware_concurrency();

    vector<thread> threads;
    atomic<long> counter(0);
    // creating threads
    for (int i = 0 ; i < hwc ; ++i)
    {
        threads.emplace_back(atomic_count_inside_circle, N/hwc, ref(counter));
    }
    // joining threads
    for(auto& th : threads) th.join();
    cout << "Pi = " << double(counter)/double(N)*4 << " time = ";
    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms - multithread - atomic" << endl;
}

int single_thread_pi_calc()
{
    long counter = 0;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1, 1);

    auto start = chrono::high_resolution_clock::now();

    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if ( x*x + y*y < 1)
            ++counter;
    }
    cout << "Pi = " << double(counter)/double(N)*4 << " time = ";

    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms - single thread" << endl;
}

int main()
{
    single_thread_pi_calc();
    multithread_pi_calc();
    multithread_pi_calc_global_counter();
    mutex_multithread_pi_calc();
}
