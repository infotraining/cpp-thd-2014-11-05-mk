#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <vector>
#include <atomic>

using namespace std;

class Service
{
public:
    virtual void run() =0;
    virtual ~Service() {}
};

class RealService : public Service
{
    string url_;
public:
    RealService(const string& url) : url_(url)
    {
        cout << "Creating service...." << endl;
        this_thread::sleep_for(1s);
        cout << "Service ready" << endl;
    }

    void run()
    {
        cout << "RealService::run" << endl;
    }
};

namespace not_working {

class ProxyService : public Service
{
    RealService* real_service_;
    string url_;
    mutex mtx;
public:
    ProxyService(const string& url) : real_service_(nullptr), url_(url)
    {
    }

    ProxyService(const ProxyService&) = delete;
    ProxyService& operator=(const ProxyService&) = delete;

    ~ProxyService()
    {
        if(real_service_)
            delete real_service_;
    }

    void run()
    {
        if (real_service_ == nullptr)
        {
            lock_guard<mutex> l(mtx);
            if (real_service_ == nullptr)
                real_service_ = new RealService(url_);
            //
        }
        real_service_->run();

    }
};
}

namespace atomics_at_work {

class ProxyService : public Service
{
    atomic<RealService*> real_service_;
    string url_;
    mutex mtx;
public:
    ProxyService(const string& url) : real_service_(nullptr), url_(url)
    {
    }

    ProxyService(const ProxyService&) = delete;
    ProxyService& operator=(const ProxyService&) = delete;

    ~ProxyService()
    {
        if(real_service_.load())
            delete real_service_;
    }

    void run()
    {
        if (real_service_.load() == nullptr)
        {
            lock_guard<mutex> l(mtx);
            if (real_service_.load() == nullptr)
                real_service_.store(new RealService(url_));
            //
        }
        (*real_service_).run();

    }
};
}

namespace call_once_at_work {

class ProxyService : public Service
{
    RealService* real_service_;
    string url_;
    mutex mtx;
    once_flag flag;
public:
    ProxyService(const string& url) : real_service_(nullptr), url_(url)
    {
    }

    ProxyService(const ProxyService&) = delete;
    ProxyService& operator=(const ProxyService&) = delete;

    ~ProxyService()
    {
        if(real_service_)
            delete real_service_;
    }

    void run()
    {
        call_once(flag, [this] { this_thread::sleep_for(1s); real_service_ = new RealService(url_);});
        real_service_->run();

    }
};
}

namespace static_at_work {

class ProxyService : public Service
{
    static RealService real_service_;
    string url_;
    mutex mtx;
    once_flag flag;
public:
    ProxyService(const string& url) : url_(url)
    {
    }

    ProxyService(const ProxyService&) = delete;
    ProxyService& operator=(const ProxyService&) = delete;

    ~ProxyService()
    {
    }

    RealService get_instance()
    {
        static RealService real_service(url_);
        return real_service;
    }

    void run()
    {
        get_instance().run();
    }
};
}

using namespace static_at_work;

int main()
{

    cout << "Hello World!" << endl;
    ProxyService service("www.wp.pl");
    cout << "After creating service" << endl;
    vector<thread> thds;
    for(int i = 0 ; i < 10 ; ++i)
        thds.emplace_back( [&] {
            service.run();
        });

    for (auto& th : thds) th.join();

    return 0;
}

