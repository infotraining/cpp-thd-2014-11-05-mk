#include <iostream>
#include <thread>
#include <stdexcept>

using namespace std;

void may_throw(exception_ptr& exptr)
{
    try
    {
        throw runtime_error("Error from may_throw");
    }
    catch(...)
    {
        exptr = current_exception();
    }
}

int main()
{
    cout << "Start" << endl;

    exception_ptr exptr;

    thread t(may_throw, ref(exptr));
    t.join();

    if ( exptr)
    {
        try
        {
            rethrow_exception(exptr);
        }
        catch(const runtime_error& e)
        {
            cout << "Catch: " << e.what() << endl;
        }

    }
    cout << "End" << endl;

    return 0;
}

