#include <iostream>
#include <thread>
#include <memory>
#include <mutex>
#include <atomic>

using namespace std;

class big_resource
{
public:
    void do_something()
    {

    }
};

std::shared_ptr<big_resource> resource_ptr;
std::mutex resource_mutex;

std::once_flag resource_flag;

void work_flag_once()
{
    std::call_once( resource_flag, []
        {
            resource_ptr.reset( new big_resource );
        }
    );

    resource_ptr->do_something();
}

std::atomic<big_resource*> atom_ptr;

void work_atom()
{
    if ( atom_ptr.load() == nullptr )
    {
        std::lock_guard<std::mutex> l( resource_mutex );
        if ( atom_ptr.load() == nullptr )
        {
            big_resource* tmp = new big_resource;
            atom_ptr.store(tmp);
        }
    }
    atom_ptr.load()->do_something();
}

void work()
{
    if ( !resource_ptr )
    {
        std::lock_guard<std::mutex> l( resource_mutex );

        if ( !resource_ptr )
        {
            resource_ptr.reset( new big_resource );
        }
    }

    resource_ptr->do_something();
}



int main()
{
    cout << "Hello World!" << endl;
    return 0;
}

