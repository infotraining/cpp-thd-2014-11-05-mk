#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>

using namespace std;

mutex cmutex;

template <typename T>
struct node
{
    node* prev;
    T data;
    node (T data, node* prev) : data(data), prev(prev)
    {}
};

template <typename T>
class atomic_stack
{
    atomic<node<T>*> head;
public:
    atomic_stack() : head(nullptr)
    {
    }

    void push(T item)
    {
        node<T>* new_node = new node<T>(item, head.load());
        //head.store(new_node); // still race...
        while(!head.compare_exchange_weak(new_node->prev, new_node));
    }

    T pop()
    {
        node<T>* old_node;
        old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->prev));
        T res = old_node->data;
        // not safe -- delete old_node;
        return res;
    }

};

void push_and_pop(atomic_stack<int>& s, int start)
{
    for (int i = start ; i < start + 1000 ; ++i)
        s.push(i);

    for (int i = start ; i < start + 1000 ; ++i)
    {
        lock_guard<mutex> l(cmutex);
        cout << s.pop() << " ; ";
    }
}

int main()
{
    cout << "Hello World!" << endl;
    atomic_stack<int> s;
    vector<thread> thds;
    thds.emplace_back(push_and_pop, ref(s), 0);
    thds.emplace_back(push_and_pop, ref(s), 1000);
    for (auto& th : thds) th.join();
    cout << endl;
    return 0;
}
