#include <iostream>
#include <thread>
#include "scoped_thread.h"
#include <mutex>
#include <atomic>

using namespace std;

const int num_iter = 10000000;
int counter = 0;

void worker()
{
    for(int i=0; i<num_iter; i++)
        counter++;
}

int test_01()
{
    auto start = chrono::high_resolution_clock::now();
    {
    scoped_thread t1(worker);
    scoped_thread t2(worker);
    }
    auto end = chrono::high_resolution_clock::now();
    float mseconds = chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << "Counter = " << counter << " time = " << mseconds << "ms" << endl;

    return 0;
}

mutex mtx;
void worker_mutex()
{
    for(int i=0; i<num_iter; i++)
    {
        lock_guard<mutex> l(mtx);
        counter++;
    }
}

void test_02()
{
    counter = 0;
    auto start = chrono::high_resolution_clock::now();
    {
    scoped_thread t1(worker_mutex);
    scoped_thread t2(worker_mutex);
    }
    auto end = chrono::high_resolution_clock::now();
    float mseconds = chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << "Counter(mutex) = " << counter << " time = " << mseconds << "ms" << endl;
}

atomic<int> atomic_counter;

void worker_atomic()
{
    for(int i=0; i<num_iter; i++)
    {
        atomic_counter++;
    }
    std::atomic_thread_fence;
}

void test_03()
{
    auto start = chrono::high_resolution_clock::now();
    {
    scoped_thread t1(worker_atomic);
    scoped_thread t2(worker_atomic);
    }
    auto end = chrono::high_resolution_clock::now();
    float mseconds = chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << "Counter(atomic) = " << atomic_counter << " time = " << mseconds << "ms" << endl;
}


int main()
{
    test_01();
    test_02();
    test_03();
}










