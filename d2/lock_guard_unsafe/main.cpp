#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

class some_data
{
    int a;

public:

    void do_something()
    {
    }
};


class data_wrapper
{
private:
    some_data data;
    std::mutex m;

public:

    template<typename Function>
    void process_data(Function func)
    {
        std::lock_guard<std::mutex> l(m);

        func(data);
    }
};

some_data* unprotected;

void mal_function(some_data& protected_data)
{
    unprotected = &protected_data;
    cout << "mal_function" <<endl;
}

data_wrapper x;

void foo()
{
    x.process_data( mal_function );
    unprotected->do_something();
}

int main()
{
    foo();

    return 0;
}

