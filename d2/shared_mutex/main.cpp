#include <iostream>
#include <string>
#include <thread>
#include <shared_mutex>
#include <vector>
#include <map>

using namespace std;

class dns_entry
{
public:
    std::string value;

    dns_entry() : value ("-empty-") {}
    dns_entry(const std::string v) : value (v) {}
};

class dns_cache
{
    std::map<std::string, dns_entry> entries;
    std::shared_timed_mutex entry_mutex;

public:

    dns_entry find_entry( std::string const& domain )
    {
        std::cout << "find_entry START" << std::endl;
        std::shared_lock<std::shared_timed_mutex> l(entry_mutex);

        auto it = entries.find(domain);

        std::cout << "find_entry END" << std::endl;
        return (it==entries.end()) ? dns_entry():it->second;
    }

    void update_add_entry(std::string const& domain, dns_entry const dns_val)
    {
        std::cout << "update_add_entry START" << std::endl;
        std::lock_guard<std::shared_timed_mutex> l(entry_mutex);
        entries[domain] = dns_val;
        std::cout << "update_add_entry END" << std::endl;
    }
};

dns_cache dns;
std::vector<std::string> values = {"www1", "xyz", "abc"};

void client()
{
    while(true)
    {
        for(const auto& v: values)
        {
            std::cout << "client " << std::this_thread::get_id() << " " <<
                         dns.find_entry(v).value << std::endl;

            std::this_thread::sleep_for( std::chrono::milliseconds(798) );
        }
    }
}

void server()
{
    for(const auto& v: values)
    {
        std::this_thread::sleep_for( std::chrono::milliseconds(1798) );

        dns_entry entry("1.1.1.1");

        dns.update_add_entry(v, entry);

        std::cout << "server - add -> " << v << std::endl;
    }
}

int main()
{
    thread t1(server);
    thread t2(client);
    thread t3(client);
    thread t4(client);
    thread t5(client);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();

    return 0;
}

