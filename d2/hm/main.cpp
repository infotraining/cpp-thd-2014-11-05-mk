#include <iostream>
#include <thread>
#include <mutex>
#include <stdexcept>
#include <climits>

using namespace std;

thread_local int v;

class hmutex
{
    std::mutex internal_mutex;
    unsigned long const h_value;
    unsigned long previous_h_value;
    static thread_local unsigned long this_hierarchy_value;

    void check()
    {
        if (this_hierarchy_value < h_value)
        {
            throw std::logic_error("mutex hierarchy error");
        }
    }

    void update()
    {
        previous_h_value = this_hierarchy_value;
        this_hierarchy_value = h_value;
    }

public:

    explicit hmutex(unsigned long value) : h_value( value),
                                           previous_h_value(0)
    {
    }

    void lock()
    {
        check();
        internal_mutex.lock();
        update();
    }

    void unlock()
    {
        this_hierarchy_value = previous_h_value;
        internal_mutex.unlock();
    }

    bool try_lock()
    {
        check();
        if ( !internal_mutex.try_lock() )
            return false;

        update();
        return true;
    }
};

thread_local unsigned long hmutex::this_hierarchy_value(ULONG_MAX);


void worker()
{
    for (int i=0; i<10; i++)
    {
        cout << hex << std::this_thread::get_id() << " " << v << endl;
        v = i;
        cout << hex << std::this_thread::get_id() << " " << v << endl;
        std::this_thread::sleep_for( std::chrono::milliseconds(100) );
    }
}


hmutex high_level_mutex(1000);
hmutex mid_level_mutex(500);
hmutex low_level_mutex(4);

void high_level_func()
{
    std::lock_guard<hmutex> lk( high_level_mutex );
}

void do_other_stuff()
{
    high_level_func();
}

void thread_a()
{
    std::lock_guard<hmutex> lk( mid_level_mutex );

    do_other_stuff();
}

int main()
{
    v = 123456;
    thread t1(thread_a);
    thread t2(thread_a);

    t1.join();
    t2.join();
}

