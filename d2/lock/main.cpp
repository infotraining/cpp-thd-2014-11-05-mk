#include <iostream>
#include <thread>
#include <mutex>

using namespace std;


class some_big_object
{};

void swap( some_big_object& lhs, some_big_object& rhs )
{
    std::cout << "SWAP" << std::endl;
}

class X
{
    typedef std::recursive_mutex Mutex;

private:
    some_big_object some_detail;
    /*mutable*/Mutex m;

public:
    X(some_big_object const& sd) : some_detail(sd){}

    void some_operation()
    {
        std::lock_guard<Mutex> lock_a(m);
    }

    friend void swap(X& lhs, X& rhs)
    {
        if (&lhs == &rhs)
            return;

        /*
        std::lock_guard<Mutex> lock_a(lhs.m);
        std::this_thread::yield();
        std::lock_guard<Mutex> lock_b(rhs.m);
        */

        std::unique_lock<Mutex> lock_a(lhs.m, std::defer_lock);
        std::unique_lock<Mutex> lock_b(rhs.m, std::defer_lock);
        std::lock( lock_a, lock_b );

        lhs.some_operation();

        //----
        swap(lhs.some_detail, rhs.some_detail);
    }

};

void work(X& a, X& b)
{
    swap(a, b);
}

int main()
{
    cout << "Hello World!" << endl;

    some_big_object sbo1;
    some_big_object sbo2;

    X x1(sbo1);
    X x2(sbo2);

    while( true )
    {
        std::thread t1(work, std::ref(x1), std::ref(x2));
        std::thread t2(work, std::ref(x2), std::ref(x1));

        t1.join();
        t2.join();
    }

    return 0;
}

