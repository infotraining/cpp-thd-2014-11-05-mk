#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

std::mutex some_mutex;

void prepare_data()
{
    cout << "prepare_data" << endl;
}

std::unique_lock<std::mutex> get_lock()
{
    std::unique_lock<std::mutex> lk( some_mutex );

    prepare_data();

    return lk;
}

int worker()
{
    cout << "Hello World!" << endl;

    std::unique_lock<std::mutex> lk( get_lock() );

    //
    //
    //

    return 0;
}


timed_mutex mtx;

void background_worker(int id, int timeout)
{
    cout << "worker " << id << endl;

    unique_lock<timed_mutex> lk(mtx, try_to_lock);

    if ( !lk.owns_lock() )
    {
        do
        {
            cout << id << " try to lock..." << endl;
            this_thread::sleep_for( chrono::milliseconds(timeout));
        }
        while( !lk.try_lock() );
    }

    //
    cout << "worker "<< id << " owns lock" << endl;
    this_thread::sleep_for( chrono::milliseconds(1100));
}

int main()
{

    thread t1(background_worker, 1, 100);
    thread t2(background_worker, 2, 200);
    thread t3(background_worker, 3, 300);

    t1.join();
    t2.join();
    t3.join();

}
